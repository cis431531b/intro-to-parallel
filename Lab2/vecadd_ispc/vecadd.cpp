#include <stdio.h>
#include <stdlib.h>

#define N 100000

#include "vecadd_ispc.h"

int main() {

  // declare arrays 
  int *A = new int [N];
  int *B = new int [N];
  int *C = new int [N];


  // initalize arrays
  for (int i = 0; i < N; ++i) {
    A[i] = i;
    B[i] = 2*i;
  }


  for(int j = 0; j < 100000; j++) {
    ispc::vecadd(A, B, C, N);
  }

  // verify output
  int flag = 1;

  for (int i = 0; i < N; ++i) {
    if (C[i] != 3*i) {
      printf("Bad value: C[%d] = %d\n", i, C[i]);
      flag = 0;
    }
  }

  if (flag)
    printf("Execution Succesful!\n");

  delete [] A;
  delete [] B;
  delete [] C;

  return 0;
}

