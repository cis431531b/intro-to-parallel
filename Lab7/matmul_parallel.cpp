#include <chrono>
#include <iostream>

#include <stdlib.h>
#include <sys/time.h>
#include <stdio.h>
#include <math.h>

#include <openacc.h>
#include <omp.h>

typedef std::chrono::high_resolution_clock Clock;

#define SIZE 1600 

int N = SIZE;  // Matrix 1: NxM
int M = SIZE;  // Matrix 2: MxP
int P = SIZE;

// [A] = [B] x [C]
void MatrixMultiplication_openacc(float * a, float * b, float * c)
{
  int i, j, k ;

  acc_init(acc_device_default);

  #pragma acc parallel loop collapse(2) copyout(a[0:(M*N)]) copyin(b[0:(M*P)],c[0:(P*N)])
  //#pragma acc parallel copyout(a[0:(M*N)]) copyin(b[0:(M*P)],c[0:(P*N)])
  {
    //#pragma acc loop gang worker collapse(2)
    for (i=0; i<M; i++){
      for (j=0; j<N; j++) {
        float sum = 0.0 ;

        #pragma acc loop seq
        for (k=0; k<P; k++) {
          sum += b[i*P+k]*c[k*N+j] ;
        }
        a[i*N+j] = sum ;
      }
    }
  }

  acc_shutdown(acc_device_default);
}


void MatrixMultiplication_openmp(float * a,float * b, float * c)
{
  int i, j, k ;
  int chunk = N/4;

  omp_set_num_threads(16);

  #pragma omp parallel shared(a,b,c,chunk) private(i,j,k)
  {
    if(omp_get_thread_num() == 0) {
      printf("Number of OpenMP threads %d\n", omp_get_num_threads());
    }

    #pragma omp for
    for (i=0; i<M; i++){
      for (j=0; j<N; j++)
      {
        float sum = 0.0 ;
        for (k=0; k<P; k++)
          sum += b[i*P+k]*c[k*N+j] ;
        a[i*N+j] = sum ;
      }
    }
  }
}

int main()
{
  float *a = (float *) malloc(M*N*sizeof(float));
  float *b = (float *) malloc(M*P*sizeof(float));
  float *c = (float *) malloc(P*N*sizeof(float));
  float *a_CPU = (float *) malloc(M*N*sizeof(float));
  float *b_CPU = (float *) malloc(M*P*sizeof(float));
  float *c_CPU = (float *) malloc(P*N*sizeof(float));

  for (int i = 0; i <  M*N; i++) {
    a[i] = (float) 0.0F;
    a_CPU[i] = (float) 0.0F;
  }
  for (int i = 0; i <  M*P; i++) {
    b[i] = (float) i;
    b_CPU[i] = (float) i;
  }
  for (int i = 0; i <  P*N; i++) {
    c[i] = (float) 1.0F;
    c_CPU[i] = (float) 1.0F;
  }

  auto t1 = Clock::now();
  MatrixMultiplication_openmp(a_CPU,b_CPU,c_CPU);
  auto t2 = Clock::now();

  std::cout << "OpenMP/CPU Elapsed time: \t"
    << std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count()
    << " milliseconds" << std::endl;

  t1 = Clock::now();
  MatrixMultiplication_openacc(a,b,c);
  t2 = Clock::now();

  std::cout << "OpenACC/GPU Elapsed time: \t"
    << std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count()
    << " milliseconds" << std::endl;

  {
    double cpu_sum = 0.0;
    double gpu_sum = 0.0;
    double rel_err = 0.0;

    for (int i=0; i<M*N; i++){
      cpu_sum += a_CPU[i]*a_CPU[i];
      gpu_sum += a[i]*a[i];
    }

    cpu_sum = sqrt(cpu_sum);
    gpu_sum = sqrt(gpu_sum);
    if( cpu_sum > gpu_sum ) {
      rel_err = (cpu_sum-gpu_sum)/cpu_sum;
    } else {
      rel_err = (gpu_sum-cpu_sum)/cpu_sum;
    }

    if(rel_err < 1e-6)
    {
      printf("Verification Successful err = %e\n", rel_err);
    }
    else
    {
      printf("Verification Fail err = %e\n", rel_err);
    }
  }

  free(a_CPU);
  free(b_CPU);
  free(c_CPU);
  free(a);
  free(b);
  free(c);

  return 0;
} 

